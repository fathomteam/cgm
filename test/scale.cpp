/**
 * \file scale.cpp
 * \brief scale, scaling and bounding box
 *
 * This program creates solid geometries -
 * compute bounding box1 then scales
 * again computes bounding box2.
 * For a sphere, cylinder and circle-surface with OCE 0.17 and OCC 7.0, the bounding box is not acceptable.
 */
#include "GeometryModifyTool.hpp"
#include "GeometryQueryTool.hpp"
#include "OCCQueryEngine.hpp"
#include "Body.hpp"
#include "InitCGMA.hpp"

#include "OCCModifyEngine.hpp"
#include "CubitCompat.hpp"
#include "TestUtilities.hpp"
// main program - initialize, then send to proper function
bool debug = false;

int main (int argc, char **argv)
{
  CubitStatus status = InitCGMA::initialize_cgma("OCC");
  if (CUBIT_SUCCESS != status) return 1;

  GeometryQueryTool *gti = GeometryQueryTool::instance();
  GeometryModifyTool *gmti = GeometryModifyTool::instance();

  int prob_type =1; // problem type 1 only works, the bounding box computation for sphere, circle surface and cylinder is off - more investigation is needed. The bug is in OCC or OCE not CGM. Tested direct OCE calls and the error persist.
  Body* body2 = NULL;
  CubitVector plane(1, 0, 0);
  if (prob_type == 1)
    body2 = gmti->brick(10, 10, 10);
  else if (prob_type == 2)
    body2 = gmti->sphere(5.0, 0 , 0, 0, 0, false);
  else if (prob_type == 3)
    body2 = gmti->cylinder(5.0, 5.0, 5.0, 5.0);
  else if (prob_type == 4)
    body2 = gmti->create_circle_surface(5.0, plane);

  // compute bounding box
  CubitBox curr_box_before = gti->bounding_box_of_bodies();

  CubitVector scale(2, 2, 2);
  CubitVector p1(0, 0, 0);
  gmti->scale(body2, p1, scale);

  // compute bounding box
  CubitBox curr_box_after = gti->bounding_box_of_bodies();

  // check if bounding boxes are identical
  double tol = 1.0/100;
  int return_val = cubit_box_identical(curr_box_after, 2*curr_box_before, tol, true);

  // save file if debug true

  if (debug){
      DLIList<RefEntity*> ref_entity_list;
      ref_entity_list.push( (RefEntity*) body2);
      const CubitString cubit_version="12.2";
      int num_ents_exported=0;
      const char *filename = "scale.brep";
      const char *filetype = "OCC";
      CubitCompat_export_solid_model(ref_entity_list, filename, filetype,
                                     num_ents_exported, cubit_version);
    }
  return (return_val-1);
}
