#undef NDEBUG

#include <vector>

#include "InitCGMA.hpp"
#include "CGMApp.hpp"
#include "CubitCompat.hpp"
#include "FacetQueryEngine.hpp"

#include "BodySM.hpp"
#include "Body.hpp"
#include "GeometryQueryTool.hpp"

#define STRINGIFY(S) XSTRINGIFY(S)
#define XSTRINGIFY(S) #S
#ifndef SRCDIR
#  define SRCDIR "."
#endif

int main( int argc, char** argv ) {

  std::vector<CubitString> args;
  CGMApp::instance()->startup(args);

  const char* filename = STRINGIFY(SRCDIR) "/unit_cube.stl";
  const char *out_file = "unit_cube.facet";

  CubitStatus status = InitCGMA::initialize_cgma();
  if (CUBIT_SUCCESS != status) return 1;
  if (argc>=2)
    filename = argv[1];
  if (argc>=3)
    out_file = argv[2];

  DLIList<CubitQuadFacet*> quad_facet_list;
  DLIList<CubitFacet*> tri_facet_list;
  DLIList<Surface*> surface_list;
  FacetQueryEngine* fqe = FacetQueryEngine::instance();
  status = fqe->import_facets(filename, CUBIT_TRUE, 135, 1.e-6, 4, CUBIT_TRUE,
    CUBIT_FALSE, CUBIT_TRUE, CUBIT_TRUE, quad_facet_list, tri_facet_list,
    surface_list, STL_FILE);

  // get bodies, then export to a facet file
  DLIList<Body*> bodies;
  GeometryQueryTool::instance()->bodies(bodies);
  PRINT_INFO(" num bodies: %d\n", bodies.size());
  //BodySM* bodysm_ptr = body_ptr->get_body_sm_ptr();
  DLIList<TopologyBridge*> model;
  for (int j=0; j<bodies.size(); j++)
  {
    BodySM* bodysm_ptr = bodies[j]->get_body_sm_ptr();
    model.append( (TopologyBridge*)bodysm_ptr);
  }
  ModelExportOptions opts;

  fqe->export_solid_model(
        model, out_file, FACET_TYPE,
        CubitString(), opts);
  PRINT_INFO( "converted stl %s to %s \n", filename,  out_file);
  if (CUBIT_SUCCESS != status) return 1;
 
  return 0;
}
